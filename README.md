# La consultoría

``` plantuml
@startmindmap
<style>
mindmapDiagram {
BackgroundColor white
    node {
        BackgroundColor white
        LineColor green
    }

    boxless {
    FontColor navy
    }

   arrow {
    LineThickness 2
    LineColor black
   }
    


}
</style>
    header
        **Instituto Tecnologico de Oaxaca**
    endheader
    caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**

* Consultoría
**_ definicion
*** Empresa de servicios profesionales de alto valor añadido
****_ se requiere
***** Conocimiento sectorial
***** Conocimiento Tecnico
***** Capacidad de plantear soluciones

** Tipologia de servicios
*** Consultoria
**** Reingenieria de procesos
***** Ayudar empresas para organizar sus actividades \n siguiendo un esquema de funcionamiento
**** Gobierno TI
***** Practicas (Guias)
******_ sobre
******* Calidad
******* Metodologia de desarollo
******* Arquitectura empresarial
**** Oficina de proyectos
***** Gestion de proyectos organizacionales
**** Analitica avanzada de datos
***** Disponibilidad de datos en tiempo real
***** Organizacion de datos
****** Big data


*** Integracion
**** Toda empresa tiene un mapa de sistemas \n dando soporte a la actividad del negocio
*****_ actualizacion de mapas
****** Desarrollos a medida
******* Utilizacion de tipos de lenguajes y tecnologias
****** Aseguramiento de la calidad E2E
******* Asegurar la calidad del software \ny la mejora continua
****** Infraestructuras
******* Uso de servicios, plataformas y nuevas tecnologias
****** Soluciones de mercado
******* Softwares pre-construidos y parametrizados \n demandados por el mercado

*** Externalizacion
**** Organizaciones de servicios asumen actividades no primordiales a otra
***** Gestion de aplicaciones
****** Delegar el mantenimiento de sus sistemas a un tercero
*******_ a traves de
******** Servicios SQA
******** Testing de aplicaciones
***** Procesos de negocio
****** Externalización de procesos de negocio

** Profesion a futuro
*** El sector crece por encima del PIB
*** El PIB crecio un 3.2% en 2015
***_ ofrece
**** Nivel de crecimiento y de carrera profesional
***** Transformacion digital
***** Industria conectada 4.0
***** Smart Cities

** Exigencia
*** Aptitud
**** Ser profesional
*****_ por su
****** Formacion y experiencia
*****_ capaces de
****** Coloaborar en un proyecto comun
****** Trabajar en equipo
****** Ofrecer honestidad
*** Actitud
**** Proactividad
***** Iniciativa y anticipacion
**** Voluntad de mejora
***** Actitud activa en contruccion de empresa
***** Aportacion de ideas de mejora
**** Responsabilidad
*** Compromiso
**** Disponibilidad total
***** En ocasiones se trabaja todo el dia
**** Nuevos retos
***** Actuar por encima de los propios conocimientos
**** Viajes

** Carrera profesional
*** Modelo de Carrera
**** Modelo de RRHH
***** Modelo de categorias
***** Plan de carrera
***** Proceso de Evaluacion
***** Plan de formacion
****_ caracteristicas
***** Flexible
***** Ajustado a las necesidades del mercado
***** Valido para diferente tipo de servicio
*** Categorias profesionales
**** Junior
***** Asistente
***** Consultor Junior
***** Consultor
**** Senior
***** Consultor Senior
***** Jefe de equipo
***** Jefe de proyecto
**** Gerente
**** Director

@endmindmap
```

# Consultoría de Software

``` plantuml
@startmindmap
<style>
mindmapDiagram {
BackgroundColor white
    node {
        BackgroundColor white
        LineColor red
    }

    boxless {
    FontColor navy
    }

   arrow {
    LineThickness 2
    LineColor black
   }

}
</style>
    header
        **Instituto Tecnologico de Oaxaca**
    endheader
    caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**

* Consultoría de Software
** Desarrolo de Sofware
***_ proceso
**** Comienza cuando un cliente tiene una necesidad de software en su negocio
**** Se da una consultoria al cliente
***** Saber que requisitos quiere para el software
***** Saber el ahorro economico con el software
***** Estudio de viabilidad
**** Diseño funcional
***** Saber la informacion de entrada \npara introducir en el sistema
***** Saber la informacion de salida que dara el sistema
***** La informacion de entrada/salida se almacena en una base de datos
***** Se hace un prototipo para el cliente
**** Diseño tecnico
***** El diseñador tecnico traslada la \ninformacion a un lenguaje de programacion
***** Se crean todos los procesos
***** Los programadores construyen el software con el lenguaje elegido
**** Prueba funcional
***** El cliente prueba que se hayan cumplido los requerimentos pedidos
** Actividades desarrolladas
***_ que se necesita para funcionar 
**** Una infraestructura
*****_ elementos
****** Ordenadores
****** Servidores
******* Almacenamiento de informacion
****** Infraestructra de comunicacion
******* Internet
****** Seguridad en la informacion
****** Software hacia organizaciones para desarrollarse
*****_ como mantenerla
****** A nivel de hardware
******* Revisar que toda la parte de infraestructura \ny comunicaciones sea accesible
****** A nivel de software
******* Revisar que los cambios normativos se adecuen
******* Revisar fallas
*** Factor de escala
**** Para empresa de menores dimensiones
***** No se requiere una estructura grande
***** Servicios mas generales
**** Para empresa de mayores dimensiones
***** Se tiene una organizacion especializada \npara un gran proyecto del cliente
***** Se tiene que conocer el sector/funcionalidad del cliente
*** Servicios a empresas
** Tendencias en el desarrollo de servicios
*** Sistema de informacion
****_ antes
***** Se guardaba la informacion en sus propios ordenadores
****_ ahora
***** Almacenar informacion en la nube
****** Informacion en servidores de grandes empresas \n(Google, Amazon)
    
@endmindmap
```
# Aplicación de la ingeniería de software

``` plantuml
@startmindmap
    header
        **Instituto Tecnologico de Oaxaca**
    endheader
    caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**

* Aplicación de la ingeniería de software
** Stratesys
*** Empresa de servicios de tecnologia
***_ señas de identidad
**** Conocimiento
**** Innovacion
**** Especializacion
**** Calidad
** Soluciones SAP
*** Cubrir todos los procesos dentro de un negocio
**** Parametrización
***** Definir el proceso de negocio punto a punto
**** Framework de Desarollo
***** Basado en un lenguaje ABAP
** Enfoque metodologico
*** Recoger requisitos del cliente
*** En que areas enfoca la aplicacion
**** Compras
**** Ventas
**** Finanzas
*** Reunirse con las areas de sistemas
*** Vision global
**** Reunirse con el director de nuestra area
*** Identificar los procesos y subprocesos \nque la empresa ejecuta dia a dia
**** Documentacion
***** Requisito Funcional
***** Analisis Funcional
***** Diseño Funcional
****** Darle un plan de proyecto al cliente
** Fases
*** Fase de analisis
**** Definir a maximo detalle el objetivo
*** Fase de construccion
**** Prototipado
***** Primera version
**** La efectividad de esta fase depende de la primera \n fase, para evitar la ampliacion de alcance
*** Fase de pruebas y formacion
**** Cada cierto tiempo se va comprobando si el proyecto \navanza conforme las necesidades del cliente
** Herramientas
*** Bibloteca de Templates
*** Herramientas de procesos
**** Analizando y documentando los procesos del negocio
****_ ejemplo
**** ARIS
*** Trazabilidad
**** Cada requisito tecnico y prueba debe tener una traza
***** Las trazas se codifican

**_ se divide el proyecto
*** Conceptores Funcionales
**** Configurar las herramientas
*** Programacion
**** Cada requisito funcional debe tener diseños tecnicos
**** El programador debe hacer pruebas
***** Se debe hacer documentacion de estas pruebas

@endmindmap
```
# Referencias

1. La consultoría

	- [La consultoría. Una profesión de éxito presente y futuro](https://canal.uned.es/video/5a6f887cb1111f81638b4570)

2. Consultoría de Software

	- [Desarrollo de Software](https://canal.uned.es/video/5a6f1733b1111f35718b4580)
    - [Actividades desarrolladas en AXPE Consulting](https://canal.uned.es/video/5a6f1734b1111f35718b4586)
    - [Tendencias en el desarrollo de servicios](https://canal.uned.es/video/5a6f1730b1111f35718b456a)
    - [Visita técnica a una consultora de software: AXPE Consulting](https://canal.uned.es/video/5a6f1734b1111f35718b458b)

3. Aplicación de la ingeniería de software

	- [Stratesys: Aplicación de la ingeniería de software](https://canal.uned.es/video/5a6f4c4eb1111f082a8b4978)
